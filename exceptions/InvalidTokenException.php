<?php

namespace app\exceptions;

use yii\base\Exception;

class InvalidTokenException extends Exception
{

    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct("Token is invalid. \t" . $message, $code, $previous);
    }
}