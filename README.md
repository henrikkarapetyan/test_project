
DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      exceptions/         containes project exceptions
      jobs                containes jobs 
      migrations          containes migrations
      services            containes services
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources

#How to install and run project
1)go to project root directory and open terminal and paste this commands
```
composer install
```
2)open /confi/db.php file and change db connection properties
after that run this command for db migration
```
php yii migrate/up
```
3) open /config/console.php and replace rabbitmq conection properties (dsn)
after that run this command
```bash
php yii queue/listen
```
4)open another tab  of terminal and go to project root type this command 
```bash
php yii order-maker
```

##That's  all!!
