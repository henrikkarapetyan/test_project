<?php


namespace app\jobs;


use app\services\OrderService;
use yii\base\BaseObject;

class OrderJob extends BaseObject implements \yii\queue\JobInterface
{
    public $token;

    public function execute($queue)
    {
        $orderService = new OrderService();
        $orderService->verifyAndParseData($this->token);
    }
}