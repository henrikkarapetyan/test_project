<?php


namespace app\jobs;


use app\models\UserMany;
use yii\base\BaseObject;

class UserManyComputeJob extends BaseObject implements \yii\queue\JobInterface
{

    public $user_id;
    public $sum;

    public function execute($many_queue)
    {
        $user_many = UserMany::findOne(['user_id' => $this->user_id]);
        if ($user_many == null){
            $user_many = new UserMany();
            $user_many->user_id = $this->user_id;
        }
        $user_many->sum = $user_many->sum + $this->sum;

        $user_many->save();

    }
}