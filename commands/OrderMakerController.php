<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\jobs\OrderJob;
use app\jobs\UserManyComputeJob;
use app\services\OrderService;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OrderMakerController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $orderService = new OrderService();
        $orders_data_token = $orderService->generateUserOrderData();
        \Yii::$app->queue->push(new OrderJob(['token' => $orders_data_token]));
        return ExitCode::OK;
    }

    public function actionDebug()
    {
        \Yii::$app->many_queue->push(new UserManyComputeJob([
            'user_id' => 4,
            "sum" => 5
        ]));
    }
}
