<?php

use yii\db\Migration;

/**
 * Class m200821_052953_user_order_transaction
 */
class m200821_052953_user_order_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_order_transaction', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'sum' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200821_052953_user_order_transaction cannot be reverted.\n";
        $this->dropTable("user_order_transaction");
        return false;
    }
}
