<?php

use yii\db\Migration;

/**
 * Class m200821_041439_user_order
 */
class m200821_041439_user_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('user_order', [
            'id' => $this->primaryKey(),
            'sum' => $this->float(),
            'commission' => $this->float(),
            'order_number' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200821_041439_user_order cannot be reverted.\n";
        $this->dropTable("user_order");
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200821_041439_user_order cannot be reverted.\n";

        return false;
    }
    */
}
