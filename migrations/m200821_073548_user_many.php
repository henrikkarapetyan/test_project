<?php

use yii\db\Migration;

/**
 * Class m200821_073548_user_many
 */
class m200821_073548_user_many extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_many', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'sum' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200821_073548_user_many cannot be reverted.\n";
        $this->dropTable('user_many');
        return false;
    }
}
