<?php

namespace app\services;

use app\exceptions\InvalidTokenException;
use app\jobs\UserManyComputeJob;
use app\models\UserOrder;
use app\models\UserOrderTransaction;

class OrderService
{
    public function generateUserOrderData()
    {
        $orders_count = random_int(1, 10);
        $active_orders = [];
        for ($i = 0; $i < $orders_count; $i++) {
            $user_order = new UserOrder();
            $user_order->order_number = random_int(1, 20);
            $user_order->commission = (random_int(5, 20) / 10);
            $user_order->sum = random_int(10, 500);
            $user_order->save();
            $active_orders[] = $user_order;
        }

        return $this->encodeUserOrdersData($active_orders);
    }

    private function encodeUserOrdersData($orders)
    {
        $orders_encoded_array = [];
        foreach ($orders as $order) {
            $orders_encoded_array[] = [
                "id" => $order->id,
                "sum" => $order->sum,
                "commission" => $order->commission,
                "order_number" => $order->order_number
            ];

        }
        $encoded_data = base64_encode(json_encode($orders_encoded_array));
        $fingerprint = hash_hmac("sha256", $encoded_data, "key");
        return $encoded_data . '-' . $fingerprint;
    }

    public function debug()
    {
        return base64_decode("eyJpZCI6MTYsInN1bSI6MzExLCJjb21taXNzaW9uIjoxLjMsIm9yZGVyX251bWJlciI6MTV9");
    }

    public function verifyAndParseData($token)
    {
        $token_parts = explode("-", $token);
        if (count($token_parts) < 2 || count($token_parts) > 2) {
            throw new InvalidTokenException();
        }

        $data_part = $token_parts[0];
        $fingerprint_part = $token_parts[1];
        if (!$this->checkIsFingerPrintIsValid("key", $data_part, $fingerprint_part)) {
            throw new InvalidTokenException();
        }

        $parsed_order_data = json_decode(base64_decode($data_part));
        foreach ($parsed_order_data as $parsed_order) {
            $user_order = UserOrder::ConvertToObject($parsed_order);
            if (!$user_order->validate()) {
                throw new InvalidTokenException();
            }
            $userOrderTransaction = new UserOrderTransaction();
            $userOrderTransaction->user_id = $user_order->order_number;
            $userOrderTransaction->sum = $user_order->sum - ($user_order->commission / $user_order->sum * 100);
            if ($userOrderTransaction->save()) {
                \Yii::$app->queue->push(new UserManyComputeJob([
                    'user_id' => $userOrderTransaction->user_id,
                    "sum" => $userOrderTransaction->sum
                ]));
            }
        }
    }


    private function checkIsFingerPrintIsValid($key, $data, $fingerprint_line)
    {
        $fingerprint_line_new = hash_hmac("sha256", $data, $key);
        return $fingerprint_line_new === $fingerprint_line;
    }
}