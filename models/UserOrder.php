<?php

namespace app\models;

/**
 * This is the model class for table "user_order".
 *
 * @property int $id
 * @property int|null $sum
 * @property float|null $commission
 * @property int|null $order_number
 */
class UserOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_number'], 'integer', 'min' => 1, 'max' => 20],
            [['sum'], 'integer', 'min' => 10, 'max' => 500],
            [['commission'], 'number', 'min' => 0.5, 'max' => 2.0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'commission' => 'Commission',
            'order_number' => 'Order Number',
        ];
    }


    public static function ConvertToObject($parsed_order_data)
    {
        $user_order = new self();
        $user_order->order_number = $parsed_order_data->order_number;
        $user_order->commission = $parsed_order_data->commission;
        $user_order->sum = $parsed_order_data->sum;

        return $user_order;
    }
}
